define(function( require, exports, module ) {
/*
 *
 * SITE CONFIG
 * This is the site configuration file. jdunklebarger.com uses this configuration file to determine the number of galleries
 * to display, the photos in those galleries, etc.
 * To add a gallery or to add a photo to a gallery, you must upload the photo to the directory ../images/gallery/, and then 
 * add that photo here along with the photo's descripttive information.
 *
 */
	 
	return {
		"header": {
			"type": "header",
			"nav": false,
			"poster":"/app/images/gallery/bed-050.jpg",
			"primary": "<span>j</span>dunklebarger",
			"secondary":"brooklyn<span>ny</span>"
		},
		"galleries": [
/* TO ADD A GALLERY, ADD HERE.
 * SAMPLE GALLERY TEMPLATE:
 
 			{
			"title": "Name of Gallery",
			"navTitle": "Short Gallery Name", //OPTIONAL -- will use title if not provided
			"nav": true,
			"id": "ShortNameOfGallery", //this can only be 1 word 
			"images": [
					{
						"href":"images/gallery/Georgie-007.jpg",// location of photo in directory
						"description":"",						// description of photo to be displayed. e.g., "walnut"
						"title":"",								// name of item being displayed, e.g., "Credenza"
						"thumbnail": "images/gallery/Georgie-007.jpg" //the thumbnail of the photo
					},
					// and repeat...
					{
						"href":"images/gallery/img565.jpg",
						"description":"",
						"alt":"",
						"thumbnail": "images/gallery/img565.jpg"
					}						
				]
		    },// comma here if more than 1 gallery, then repeat gallery below....
 */

			{
			"title": "Original Work",
			"nav": true,
			"id": "original",
			"images": [
					{
						"href":"images/gallery/photo06.jpg",
						"title":"credenza",
						"description":"solid walnut, french polish, hand-forged hardware",
						"thumbnail":"images/gallery/photo06.jpg"
					},
					{
						"href":"images/gallery/bed-000.jpg",
						"title":"bed",
						"description":"",
						"thumbnail":"images/gallery/bed-000.jpg"
					},
					{
						"href":"images/gallery/tree-chair-002.jpg",
						"title":"chair",
						"description":"",
						"thumbnail":"images/gallery/tree-chair-002.jpg"
					},
					{
						"href":"images/gallery/tree-chair-003.jpg",
						"title":"chair",
						"description":"",
						"thumbnail":"images/gallery/tree-chair-003.jpg"
					},
					{
						"href":"images/gallery/tree-chair-001.jpg",
						"title":"chair",
						"description":"",
						"thumbnail":"images/gallery/tree-chair-001.jpg"
					},
					{
						"href":"images/gallery/Georgie-007-color.jpg",
						"title":"bench",
						"description":"solid walnut",
						"thumbnail": "images/gallery/Georgie-007.jpg"
					},
					{
						"href":"images/gallery/mirror-000.jpg",
						"title":"mirror",
						"description":"",
						"thumbnail":"images/gallery/mirror-000.jpg"
					},
					{
						"href":"images/gallery/img565.jpg",
						"title":"media shelf",
						"description":"solid wenge, shelves in cherokee red",
						"thumbnail": "images/gallery/img565.jpg"
					},	
					{
						"href":"images/gallery/bookcase.jpg",
						"title":"bookcase",
						"description":"",
						"thumbnail": "images/gallery/bookcase.jpg"
					}
				]
		    },
			{
			"title": "Custom Work",
			"nav": true,
			"id": "custom",
			"images" : [
					{
						"href":"images/gallery/cubes.jpg",
						"title":"cube seating",
						"description":"",
						"thumbnail": "images/gallery/cubes.jpg"
					},
					{
						"href":"images/gallery/table-000.jpg",
						"title":"",
						"description":"",
						"thumbnail":"images/gallery/table-000.jpg"
					},
					{
						"href":"images/gallery/wall.jpg",
						"title":"paneled wall",
						"description":"",
						"thumbnail": "images/gallery/wall.jpg"
					},
					{
						"href":"images/gallery/table.jpg",
						"title":"table made from repurposed wood",
						"description":"",
						"thumbnail": "images/gallery/table.jpg"
					},
					{
						"href":"images/gallery/sign.jpg",
						"title":"hand-carved sign",
						"description":"",
						"thumbnail": "images/gallery/sign.jpg"
					},
					{
						"href":"images/gallery/console-000.jpg",
						"title":"",
						"description":"",
						"thumbnail":"images/gallery/console-000.jpg"
					},
					{
						"href":"images/gallery/photo04.jpg",
						"title":"floating shelf",
						"description":"",
						"thumbnail": "images/gallery/photo04.jpg"
					}			  ]	
		  },
		  {
			"title": "Antique & Vintage Restoration",
			"navTitle": "Antique Restoration",
			"nav": true,
			"id": "antique",
			"images" : [	
					{
						"href":"images/gallery/foldingchairs.jpg",
						"title":"folding chairs",
						"description":"plywood",
						"thumbnail": "images/gallery/foldingchairs.jpg"
					}
			  ]	
		  }
		],
		"modules": [
			{
				"display": true,
				"nav": true,
				"type": "shows",
				"title":"Shows",
				"shows": [
			/*	{
					"date":"9/14/2014",
					"name":"Brooklyn Crafts Show",
					"description": "Description of Brooklyn Crafts Show",
					"image": "images/gallery/mediashelfside.jpg"
		
				},*/
				{
					"date":"Oct 19-20 2013",
					"name":"The Factory Floor: Makers Market Furniture Showcase, Industry City",
					"titleLink": "",
					"description": "Pop-up marketplace featuring furniture and home goods products from more than 40 independent, innovative designers and manufacturers based in New York City.",
					"moreLink": "http://factoryfloorbrooklyn.com/furniture-makers/"
				},
				{
					"date":"Dec 5 2011 - Jan 21 2012",
					"name":"Craftforms 2011",
					"titleLink": "",
					"description": "17th Annual Juried Exhibition of Contemporary Craft. Meet the artists December 3, 2011 between 1 and 3. The Wayne Gallery - 413 Maplewood Avenue, Wayne, PA 19087 (Mainline Philadelphia).",
					"moreLink": "http://www.craftforms.com/2011.htm"
				}
				]				
			},
			{
				"display": true,
				"nav": true, 
				"type": "contact",
				"title": "Contact",
				"content":"<p>Jeremy Dunklebarger is a furniture designer and fabricator based in Brooklyn, New York. He received his BFA from the University of the Arts in Philadelphia. A classically trained woodworker with over thirty years of experience, Jeremy also performs antique restoration and repair.</p><p>To inquire about original designs or commission work, contact him at <a href=\"javascript:location='mailto:\u006a\u0065\u0072\u00065\u006d\u0079\u0040\u006a\u0064\u0075\u006e\u006b\u006c\u0065\u0062\u0061\u0072\u0067\u0065\u0072\u002e\u0063\u006f\u006d';void 0\">jeremy@jdunklebarger.com</a></p>",
				"image": "images/gallery/mediashelfside.jpg"
			},
			{
				"display": true,
				"nav": false,
				"content": "<p class=\"text-muted\">All photos ©2017 Jeremy Dunklebarger | All rights reserved.</p>",
				"type": "footer"
			}
		]
	}
});