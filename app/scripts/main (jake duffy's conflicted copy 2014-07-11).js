define(function( require, exports, module ) {

	var CONFIG = require("config/site.js"),
	
		//nav
		_nav = {
			"items": []
		},
	    
	    //galeries
	    _galleries = CONFIG.galleries,
	    _galleryTemplate = require("hgn!../packages/templates/gallery"),
	   
	    //modules
	    _modules = CONFIG.modules,
	    _moduleRegistry = {
	    	"default": require("hgn!../packages/templates/module"),
		    "shows": require("hgn!../packages/templates/upcomingshows"),
		    "contact": require("hgn!../packages/templates/contact"),
		    "header": require("hgn!../packages/templates/header"),
		    "nav": require("hgn!../packages/templates/nav"),
		    "footer": require("hgn!../packages/templates/footer")
	    }
	    
	    
	/*
	 * setup parallax 
	 */
	if ($(window).width() > 680) {
			$(window).stellar({
				verticalScrolling: true,
				horizontalScrolling: false
			});
		//	$(window).data('plugin_stellar').destroy();
		//	$(window).data('plugin_stellar').init();
		}
		else {
			//if ($(window).data('plugin_stellar') != undefined) {
			//	$(window).data('plugin_stellar').destroy();
				$('.stellar-bg').css('background-position','50% 50%');
			//}
	}
	 
	 
	 //use data to popoulate templates on the page
	 
	 //first do nav
	 	//(pass through page.json widgets getting title and associating with jumplinks)
	 //then do widgets
	 	//if widget is listed as gallery, do that
	 	
	 	
	/*
	 *
	 * SETUP GALLERIES
	 *
	 */
	 
	var blueimpIndicator = require("bower_components/blueimp-gallery/js/blueimp-gallery-indicator"), 
		blueimpModule = require("bower_components/blueimp-gallery/js/blueimp-gallery");
	
	 var thisGallery;
	 
	 if (_galleries) {
		 $(_galleries).each(function(index, gallery) {
			 
			 if (gallery.nav) {
			 	 var nav={
				 	"id":gallery.id,
				 	"display":gallery.id
			 	 };
				 _nav.items.push(nav)
			 }

			$("#galleries").append(_galleryTemplate(gallery));
			 
			 thisGallery = new blueimpModule(gallery.images,
			    {
			        container: '#'+gallery.id+'-gallery-carousel',
			        carousel: true,
			        startSlideshow: false,
			        // The tag name, Id, element or querySelector of the indicator container:
				    indicatorContainer: 'ol',
				    // The class for the active indicator:
				    activeIndicatorClass: 'active',
				    // The list object property (or data attribute) with the thumbnail URL,
				    // used as alternative to a thumbnail child element:
				    thumbnailProperty: 'thumbnail',
				    // Defines if the gallery indicators should display a thumbnail:
				    thumbnailIndicators: true
				});
			 
		 });
	 }
	 
	/*
	 *
	 * SETUP MODULES
	 *
	 */
	 
	 if (_modules) {
		 $(_modules).each(function(index, module) {
		 
		 var thisModule;
		 
		 if (module.display) {
			 if (_moduleRegistry[module.type]) {
				 //this module has a custom template
				 thisModule = _moduleRegistry[module.type];
			 } else {
				 //use default
				 thisModule = _moduleRegistry["default"];
			 }
			 
			  if (module.nav) {
			 	 var nav={
				 	"id":module.type,
				 	"display":module.type
			 	 };
				 _nav.items.push(nav)
			 }
			 
			 $("#modules").append(thisModule(module));
		}
			 
		 });
	 }
	 
	 /*
	  *
	  * SETUP NAV  
	  *
	  */
	  if (_nav.items) {
	  		console.log(_nav);
	  	  var navTpl = _moduleRegistry["nav"];
		  $("nav").append(navTpl(_nav));
		  
		  //refresh scrollspy
		  $('[data-spy="scroll"]').each(function () {
			  var $spy = $(this).scrollspy('refresh')
		  })
		  
	  }
	  
	 
	
	/*
	document.getElementById('craft-gallery').onclick = function (event) {
	    event = event || window.event;
	    var target = event.target || event.srcElement,
	        link = target.src ? target.parentNode : target,
	        container = document.getElementById('blueimp-gallery-carousel');
	        options = {index: link, event: event, container: container, carousel: true},
	        links = this.getElementsByTagName('a');
	    blueimp.Gallery(links, options);
	};
	*/
	
	/*
	bGallery(
    [{
        title: 'Banana',
        href: 'images/gallery/bed-033.jpg',
        type: 'image/jpeg',
        thumbnail: 'images/gallery/thumbnails/bed-033.jpg'
    },
    {
        title: 'Apple',
        href: 'images/gallery/bed-033.jpg',
        type: 'image/jpeg',
        thumbnail: 'images/gallery/thumbnails/bed-033.jpg'
    }],
    {
        container: '#craft-gallery-carousel',
        carousel: true,
        startSlideshow: false
    }
    );
    */


	
	
	

});